"""

"""
from gensim import corpora, models
from modules.DeGids.DeGids import FoliaCorpus

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


if __name__ == "__main__":
    CORPUS = "/mnt/scistor1/group/home/vanwissen/degids_1837_1936/"
    METAFILES = "data/metadata_auteursinformatie"


    dgcorpus = FoliaCorpus(corpuspath=CORPUS,
                           metafiles=METAFILES,
                           years=(1837,1936),
                           debug=True,
                           lemma=False,
                           gzipped=False)  # doesn't load the corpus into memory!

    dgcorpus.dictionary.save('dgcorpus.dictionary')


