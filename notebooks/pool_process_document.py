import string

def pool_process_document(document):
    year = document.year
    title = document.title
    
    tokens = [w for w in document.words()]
    
    n_tokens = len(tokens)
    n_words = len([w for w in tokens if w not in string.punctuation])
    
    return (year, n_tokens, n_words)
